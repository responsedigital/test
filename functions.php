<?php

namespace Fir\Pinecones\Test;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Test',
            'label' => 'Pinecone: Test',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "A basic shell for testing, creating, extening"
                ],
                [
                    'label' => 'Greeting',
                    'name' => 'greeting',
                    'type' => 'text'
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ]            
            ]
        ];
    }
}