<!-- Start Test -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A basic shell for testing, creating, extening -->
@endif
<div class="test"  is="fir-test">
  <script type="application/json">
      {
          "options": @json($jsonData)
      }
  </script>
  <div class="test__wrap">
      Pinecone: Test / Test
  </div>
</div>
<!-- End Test -->