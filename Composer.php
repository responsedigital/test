<?php

namespace Fir\Pinecones\Test;

use Roots\Acorn\View\Composer;
use App\Fir;


class Test extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'Test.view'
    ];
    

    /**
     * Data to be passed to view before rendering.
     * Useful for creating values not directly associated to ACF values
     *
     * @return array
     */
    public function with()
    {
        self::injectScripts();
        $data = self::parse($this->data->get('component'));

        $newData = [
            
        ];

        return array_merge($data, $newData);  
    }


    /**
     * Parses and returns the component with any data manipulation.
     * Useful for converting options into class names etc
     *
     * @return string
     */
    private function parse($component)
    {   
        // $component['test'] = 'This is a test...';
        return $component;
    }


    private static function injectScripts()
    {
        // EXPERIMENTAL ....
        /**
         * https://developer.wordpress.org/reference/functions/wp_enqueue_style/
         *
         * wp_enqueue_style( string $handle, string $src = '', array $deps = array(), string|bool|null $ver = false, string $media = 'all' )
         *
         * wp_enqueue_style('resp/slider.css', "//cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.1/tiny-slider.css", false, null);
         *
         */

         /**
         * https://developer.wordpress.org/reference/functions/wp_enqueue_script/
         *
         * wp_enqueue_script( string $handle, string $src = '', array $deps = array(), string|bool|null $ver = false, bool $in_footer = false )
         *
         * wp_enqueue_script('resp/slider.js', "//cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.1/min/tiny-slider.js", false, null, true);
         *
         */
    }    
}
